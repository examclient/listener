object FrmMain: TFrmMain
  Left = 844
  Top = 494
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #30417#21548#22120
  ClientHeight = 443
  ClientWidth = 656
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 641
    Height = 409
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#20116#31508#36755#20837#27861
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 424
    Width = 656
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 220
      end
      item
        Width = 50
      end>
  end
  object IdTCPServer1: TIdTCPServer
    Bindings = <>
    CommandHandlers = <>
    DefaultPort = 0
    Greeting.NumericCode = 0
    MaxConnectionReply.NumericCode = 0
    OnConnect = IdTCPServer1Connect
    OnExecute = IdTCPServer1Execute
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 0
    Left = 256
    Top = 304
  end
  object PopupMenu1: TPopupMenu
    Left = 168
    Top = 128
    object pmemu0: TMenuItem
      Caption = #26174#31034
      OnClick = pmemu0Click
    end
    object pmemu3: TMenuItem
      Caption = #20851#20110
      OnClick = pmemu3Click
    end
    object pmemu4: TMenuItem
      Caption = '-'
    end
    object pmemu5: TMenuItem
      Caption = #28155#21152#24320#26426#21551#21160
      OnClick = pmemu5Click
    end
    object pmemu6: TMenuItem
      Caption = #21024#38500#24320#26426#21551#21160
      OnClick = pmemu6Click
    end
    object pmemu2: TMenuItem
      Caption = '-'
    end
    object pmemu1: TMenuItem
      Caption = #36864#20986
      OnClick = pmemu1Click
    end
  end
end
