# listener

#### 介绍
考场监听器，一般安装在机房的考试机上，运行后常驻内存，任务栏托盘处会有图标，可远程接受管理机（Manager）的各项指令，用于启动或关闭考试机。
在本产品发布时，监听器没有单独使用，而是合并到客户端（Client）一起，简化使用。


#### 开发工具
采用 Pascal 语言开发，推荐使用 Lazarus，可兼容 Delphi 7.0 及以上版本


#### 安装教程

绿色免安装，解压即用


更多内容参见： [https://gitee.com/examclient/manager](https://gitee.com/examclient/manager)




2021.10.07