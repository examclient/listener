unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, StrUtils, Registry, 
  Dialogs, StdCtrls, IdBaseComponent, IdComponent, IdTCPServer, ComCtrls, ShellAPI, IniFiles,
  Menus;

const 
  WM_TRAYMSG = WM_USER + 101;                   //自定义托盘消息
  Version = '1.0.1';                            //监听器版本

type
  TFrmMain = class(TForm)
    Memo1: TMemo;
    IdTCPServer1: TIdTCPServer;
    StatusBar1: TStatusBar;
    PopupMenu1: TPopupMenu;
    pmemu1: TMenuItem;
    pmemu2: TMenuItem;
    pmemu3: TMenuItem;
    pmemu0: TMenuItem;
    pmemu4: TMenuItem;
    pmemu5: TMenuItem;
    pmemu6: TMenuItem;
    procedure Logger(log: string);
    procedure LogMemo(log: string);
    procedure LogFile(log: string);
    procedure IdTCPServer1Connect(AThread: TIdPeerThread);
    procedure IdTCPServer1Execute(AThread: TIdPeerThread);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure pmemu1Click(Sender: TObject);
    procedure pmemu3Click(Sender: TObject);
    procedure AppMutex();
    procedure pmemu0Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure pmemu5Click(Sender: TObject);
    procedure pmemu6Click(Sender: TObject);
  private
    { Private declarations }
    //图标驻留任务栏托盘
    procedure WMTrayMsg(var Msg: TMessage);message WM_TRAYMSG;
    procedure WMSysCommand(var Msg: TMessage);message WM_SYSCOMMAND;
    //执行管理机命令
    procedure ExeSystem(cmd: string);
    procedure ExeClientOpen(seat: string; url: string; name: string );
    procedure ExeCommand(cmd: string);
    //注册开机启动
    procedure SetAutoRun(flag: boolean);
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;
  NotifyIcon: TNotifyIconData;                    //定义托盘图标结构体

implementation

uses Fun;

{$R *.dfm}

//窗体创建时
procedure TFrmMain.FormCreate(Sender: TObject);
begin
  //启动互斥，防止重复启动
  Self.AppMutex();
  //显示状态栏内容
  StatusBar1.Panels[0].Text := '本机 IP：' + Fun.LocalIP;
  StatusBar1.Panels[0].Width:= 220;
  StatusBar1.Panels[1].Text := '监听器版本：' + Version;
  StatusBar1.Panels[1].Width:= 200;
  StatusBar1.Panels[2].Text := '当前日期：' + FormatDateTime('yyyy-MM-dd',Now());
  StatusBar1.Panels[2].Width:= 210;
  //定义托盘图标的结构体
  with NotifyIcon do 
  begin 
    cbSize := SizeOf(TNotifyIconData); 
    Wnd := Self.Handle;
    uID := 1; 
    uFlags := NIF_ICON + NIF_MESSAGE + NIF_TIP;   //图标、消息、提示信息 
    uCallbackMessage := WM_TRAYMSG; 
    hIcon := Application.Icon.Handle; 
    szTip := '监听器';
  end; 
  Shell_NotifyIcon(NIM_ADD,@NotifyIcon);
  //显示日志并记录
  Memo1.Clear;
  Self.Logger('监听器启动，本机IP为 ' + Fun.LocalIP);
  //启动TCP Server
  try
    IdTCPServer1.DefaultPort := 2001;
    IdTCPServer1.Active := true;
    Self.Logger('TCP Server 启动成功，端口号为 2001 ');
  except
    Self.Logger('TCP Server 启动【失败】，端口号为 2001 ');
  end;
  //不显示主窗体，仅在托盘显示
  Application.ShowMainForm := False;
  //添加开机启动
  Self.SetAutoRun(true);
end;

//确保程序不被重复启动
procedure TFrmMain.AppMutex();
var
  Mutex: THandle;
begin
  Mutex := CreateMutex(nil, True, PChar(Application.Title));
  if GetLastError = ERROR_ALREADY_EXISTS then
  begin
    Application.MessageBox('监听器已经在运行，请勿重复打开','系统提示', MB_ICONERROR);
    ReleaseMutex(Mutex);   //释放资源
    Application.Terminate;
  end;
end;

//日志记录两处
procedure TFrmMain.Logger(log: string);
var
  str : string;
begin
  str := FormatDateTime('yyyy-MM-dd hh:nn:ss',Now()) + '    ' + log;
  Self.LogMemo(str);
  Self.LogFile(str);
end;

//日志记录显示在MEMO中
procedure TFrmMain.LogMemo(log: string);
begin
  Memo1.Lines.Add(log);
end;

//日志记录写入FILE中
procedure TFrmMain.LogFile(log: string);
var
  txt : TextFile;
  path, name : string;
begin
  name := FormatDateTime('yyyy-MM-dd',Now());
  path := ExtractFilePath(Application.ExeName) + 'Listener-Log-' + name + '.txt';
  //如果不存在就创建
  if not FileExists(path) then
  begin
    Assignfile(txt,path);     //指定文件路径
    rewrite(txt);             //创建并打开一个新文件
    Closefile(txt);           //关闭打开的文件
  end;
  //追加到文件末尾
  AssignFile(txt,path);
  Append(txt);                //以追加的方式打开文件
  Writeln(txt, log);          //写文件
  CloseFile(txt);             //关闭打开的文件
end;

//当有电脑接入时
procedure TFrmMain.IdTCPServer1Connect(AThread: TIdPeerThread);
var
  ip : string;
begin
  ip := AThread.Connection.Socket.Binding.PeerIP;
  Self.Logger('有管理机成功接入，管理机 IP 为 ' + ip);
end;

//当有指令发来时
procedure TFrmMain.IdTCPServer1Execute(AThread: TIdPeerThread);
var
  str : string;
  cmd,seat,url,cname : string;               //不能用name作为变量
  c1,c2,c3,c4 : integer;                     //格式：#CLIENT_OPEN*24^https://www.domain.com/$考试名称
begin
  with AThread.Connection do
  begin
    str := readln('',3000);
    Self.Logger('接收到管理机指令，指令内容为 ' + str);
    {
    #CLIENT_SCAN*
    #CLIENT_REFRESH*
    #CLIENT_OPEN*24^
    #CLIENT_CLOSE*
    #SYSTEM_REBOOT*
    #SYSTEM_SHUTDOWN*
    }
    if length(str) > 10 then
    begin
      WriteLn('YES');
      //找到特殊字符，并取出之间的命令名称
      c1 := pos('#', str);
      c2 := pos('*', str);
      c3 := pos('^', str);
      c4 := pos('$', str);
      //取出相关参数     
      cmd  := MidStr(str, c1+1, c2-2);
      //记录日志
      Self.Logger('指令解析后，名称是 ' + cmd);
      //处理系统命令
      if (cmd = 'SYSTEM_REBOOT') or (cmd = 'SYSTEM_SHUTDOWN') then
      begin
        Self.ExeSystem(cmd);
        exit;
      end;
      //处理系统命令
      if (cmd = 'CLIENT_OPEN') then
      begin
        seat := MidStr(str, c2+1, c3-c2-1);
        url  := MidStr(str, c3+1, c4-c3-1);
        cname := MidStr(str, (c4+1), (length(str)-4));
        Self.ExeClientOpen(seat, url, cname);
        exit;
      end;
      //处理业务命令
      if (cmd = 'CLIENT_REFRESH') or (cmd = 'CLIENT_CLOSE') then
      begin
        Self.ExeCommand(cmd);
        exit;
      end;   
    end;   
  end;    
end;

//执行管理机发来的 SYSTEM_REBOOT 和 SYSTEM_SHUTDOWN
procedure TFrmMain.ExeSystem(cmd: string);
var
  hToken: THandle;
  tkp: TOKEN_PRIVILEGES;
  ReturnLength: DWord;
begin
  OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY,hToken);
  LookupPrivilegeValue(nil,'SeShutdownPrivilege',tkp.Privileges[0].Luid);
  //设定权限为1
  tkp.PrivilegeCount := 1;
  tkp.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
  //得到权限
  AdjustTokenPrivileges(hToken, FALSE, tkp, 0,nil,ReturnLength);
  Self.Logger('马上执行系统命令 ' + cmd);
  //命令：重启计算机
  if cmd = 'SYSTEM_REBOOT' then
    ExitWindowsEx(EWX_REBOOT OR EWX_POWEROFF, 0);
  //命令：关闭计算机
  if cmd = 'SYSTEM_SHUTDOWN' then
    ExitWindowsEx(EWX_SHUTDOWN or EWX_POWEROFF, 0);
end;

//执行管理机发来的 CLIENT_OPEN
procedure TFrmMain.ExeClientOpen(seat: string; url: string; name: string );
var
  Inifile: TIniFile;                  
  password, lock : string;
begin
  //先写本地配置文件
  Inifile := nil;
  try
    Inifile := TIniFile.Create(ExtractFilePath(ParamStr(0))+'./Client.ini');        //客户端必须与监听器是同级目录
  except
    Self.Logger('执行失败，CLIENT_OPEN 执行有误，./Client.ini 找不到');             //原来的监听器和客户端分别是Listener和Client，但CEF需要依赖包，只能同级方可启动，故合并目录
  end;
  //找到剩余 2 项，用于回写（因为这2项暂不支持管理机设置）
  password := Inifile.ReadString('CONFIG', 'PASSWORD', '123');
  lock := Inifile.ReadString('CONFIG', 'LOCK', 'OFF');
  //删除 5 项
  Inifile.DeleteKey('CONFIG', 'URL');
  Inifile.DeleteKey('CONFIG', 'NAME');
  Inifile.DeleteKey('CONFIG', 'PASSWORD');
  Inifile.DeleteKey('CONFIG', 'LOCK');    
  Inifile.DeleteKey('CONFIG', 'SEAT');
  //重写全部 5 项
  if url = 'Client.html' then
  begin
    url := ExtractFileDir(Application.ExeName);                                          //当前路径
    url := ExtractFileDir(url) + '\Client\Client.html';                                  //C:\gitee\examclient\Client\Client.html
    url := 'file:///' + StringReplace (url, '\', '/', [rfReplaceAll, rfIgnoreCase]);     //file:///C:/gitee/examclient/Client/Client.html
    url := url + '?seat=' + seat;
  end;
  Inifile.WriteString('CONFIG', 'URL', url );
  Inifile.WriteString('CONFIG', 'NAME', UTF8Encode(name) );            //要转为UTF编码，否则在考试客户端上会有乱码
  Inifile.WriteString('CONFIG', 'LOCK', lock );
  Inifile.WriteString('CONFIG', 'PASSWORD', password );
  Inifile.WriteString('CONFIG', 'SEAT', seat );
  Inifile.Free;
  Self.Logger('重写INI文件完成，URL - ' + url + '， NAME - ' + name + '，SEAT - ' + seat );
  //然后执行启动
  //Winexec(PChar(cmd),sw_Normal);                                                          //Winexec其实也行，但只能执行EXE文件
  shellexecute(handle,'open',pansichar('..\Client\Client.exe'),nil,nil,sw_shownormal);      //客户端必须与监听器是同级目录
  Self.Logger('已经执行业务命令 CLIENT_OPEN');
end;

//执行管理机发来的 CLIENT_REFRESH 和 CLIENT_CLOSE
procedure TFrmMain.ExeCommand(cmd: string);
var
  h:Thandle;
begin
  //命令：关闭考试机
  if cmd = 'CLIENT_CLOSE' then
  begin
    h := findWindow(nil, 'Client');   //得到客户端的句柄
    sendMessage(h,WM_CLOSE,0,0);              //关闭指定的其他程序或窗体
  end;
  //命令：刷新考试机
  if cmd = 'CLIENT_REFRESH' then
  begin
    //刷新命令暂不用作处理
  end;
  //命令：扫描考试机
  if cmd = 'CLIENT_SCAN' then
  begin
    //扫描命令暂不用作处理
  end;
  //记录日志
  Self.Logger('已经执行业务命令 ' + cmd);
end;

//截获窗体最小化消息，最小化到托盘
procedure TFrmMain.WMSysCommand(var Msg: TMessage);
begin 
  if Msg.WParam = SC_ICON then 
    Self.Visible := False 
  else 
    DefWindowProc(Self.Handle,Msg.Msg,Msg.WParam,Msg.LParam); 
end;

//自定义托盘消息
procedure TFrmMain.WMTrayMsg(var Msg: TMessage); 
var 
  p: TPoint; 
begin 
  case Msg.LParam of 
    //WM_LBUTTONDOWN: Self.Visible := True; //点击左键显示窗体（关闭，改为菜单）
    WM_RBUTTONDOWN: 
      begin
        SetForegroundWindow(Self.Handle);   //把窗口提前 
        GetCursorPos(p);
        PopupMenu1.Popup(p.X,p.Y);
      end; 
  end;
end;

//窗体销毁
procedure TFrmMain.FormDestroy(Sender: TObject);
begin
  //删除托盘图标
  Shell_NotifyIcon(NIM_DELETE,@NotifyIcon);
end;

//菜单 - 显示
procedure TFrmMain.pmemu0Click(Sender: TObject);
begin
  Self.Visible := True; 
end;

//菜单 - 关于
procedure TFrmMain.pmemu3Click(Sender: TObject);
begin
  ShowMessage('考场管理系统 - 监听器，当前版本 ' + Version);
end;

//菜单 - 退出
procedure TFrmMain.pmemu1Click(Sender: TObject);
begin
  application.Terminate;
end;

//菜单 - 添加开机启动
procedure TFrmMain.pmemu5Click(Sender: TObject);
begin
  Self.SetAutoRun(true);
end;

//菜单 - 删除开机启动
procedure TFrmMain.pmemu6Click(Sender: TObject);
begin
  Self.SetAutoRun(false);
end;
    
//避免关闭
procedure TFrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  Self.Visible := false;     //隐藏
  canclose:=False;           //不关闭
end;

//设置为开机自启动
procedure TFrmMain.SetAutoRun(flag: boolean);
var
   Reg:TRegistry;
begin
   Reg:=TRegistry.Create;
   try
     Reg.RootKey := HKEY_CURRENT_USER;     //将根键设置为 HKEY_LOCAL_MACHINE 或 HKEY_CURRENT_USER（所需权限较低）
     Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run',true);   //打开一个键
     if flag then
     begin
        Reg.WriteString('Listener监听器', ExpandFileName(ParamStr(0)));     //在Reg这个键中写入数据名称和数据数值
        Self.Logger('Listener监听器 - 已添加到开机自启动');
     end
     else
     begin
        Reg.DeleteValue('Listener监听器');
        Self.Logger('Listener监听器 - 已删除开机自启动');
        ShowMessage('恭喜，成功删除开机自启动！注意：下次手工启动后，系统将自动添加到开机启动'); 
     end;
     Reg.CloseKey;   
   finally
     Reg.Free;
   end;
end;


end.
